package com.accenture.vf.qrcode.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public enum TransactionType {

    UPI, Card;

    private static final Set<String> transactionTypeSet = new LinkedHashSet<String>(TransactionType.values().length);

    static {
        for (TransactionType transactionType : TransactionType.values())
            transactionTypeSet.add(transactionType.name());
    }

    public static boolean contains(String value) {
        System.out.println(transactionTypeSet);
        return transactionTypeSet.contains(value);
    }
}
