package com.accenture.vf.qrcode.utility;

public class Constant {
    public static final String SOURCE = "WEB-QR";
    public static final String MERCHENT_UPI_ID = "atulbyale@hdfcbank";
    public static final String MERCHENT_NAME = "BULLET-PAY-QR";
    public static final String INITAL_STATUS = "Initiated";
    public static final String CURRENCY = "INR";
    public static final String MERCHENT_DESCRIPTION = "MERCHENT DESCRIPTION";
    public static final Boolean SMS_STATUS_MODE = true;
    public static final Integer DELAY = 30;
}
