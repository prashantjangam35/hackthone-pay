package com.accenture.vf.qrcode.service;

import com.accenture.vf.qrcode.entity.Plan;
import com.accenture.vf.qrcode.repository.PlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanService {
    @Autowired
    private PlanRepository planRepository;

    public Plan createPlan(Plan plan) {
        return planRepository.save(plan);
    }

    public List<Plan> getallplans() {
        return planRepository.findAll();
    }
}
