package com.accenture.vf.qrcode.controller;

import com.accenture.vf.qrcode.SmsModel;
import com.accenture.vf.qrcode.entity.Transaction;
import com.accenture.vf.qrcode.entity.TransactionStatus;
import com.accenture.vf.qrcode.request.TransactionDTO;
import com.accenture.vf.qrcode.service.ITransactionService;
import com.accenture.vf.qrcode.utility.Constant;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    public boolean trasactionState = false;
    @Autowired
    private ITransactionService iTransactionService;
    @Autowired
    private RestTemplate restTemplate;

    @PostMapping(value = "/create")
    private ResponseEntity<Transaction> createTransaction(@RequestBody TransactionDTO transactionDTO) {
        Transaction transaction = iTransactionService.createTransaction(transactionDTO);
        return new ResponseEntity<Transaction>(transaction, HttpStatus.CREATED);
    }

    @PostMapping(value = "/sms")
    public ResponseEntity<?> createTransaction(@RequestBody SmsModel sms) {
        log.info("=======" + sms);


        System.out.println("mss== : " + sms.getMessage());
        String[] arrOfStr = sms.getMessage().split(" ");

        String amount = arrOfStr[3];
        String payerId = arrOfStr[15];
        String actualTransactionReference = arrOfStr[20].replace(").", "");
        Transaction transaction = null;
        if (StringUtils.hasText(payerId) && StringUtils.hasText(actualTransactionReference)) {
            transaction = iTransactionService.getTransactionByAmount(amount);
            if (null != transaction) {
                transaction.setStatus(TransactionStatus.Completed);
                transaction.setPayerId(payerId);
                transaction.setActualTransactionReference(actualTransactionReference);
                transaction.setStatus(TransactionStatus.Completed);
                transaction = iTransactionService.update(transaction);
            } else {
                new ResponseEntity<String>("NO DATA FOUND", HttpStatus.OK);
            }
        }
        return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);
    }

    @GetMapping("/status/{transactionReference}")
    public ResponseEntity<?> checkStatus(@PathVariable("transactionReference") String transactionReference) throws InterruptedException {
        trasactionState = true;
        if (Constant.SMS_STATUS_MODE) {
            return checkWithSms(transactionReference);
        } else {
            log.info(String.valueOf(LocalDateTime.now()));
            try {
                TimeUnit.SECONDS.sleep(Constant.DELAY);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            log.info(String.valueOf(LocalDateTime.now()));
            return checkStatusWithoutSms(transactionReference);
        }
    }

    private ResponseEntity<?> checkWithSms(String transactionReference) {
        Transaction transaction = iTransactionService.getTransaction(transactionReference);
        if (null != transaction && transaction.getStatus().equals(TransactionStatus.Completed)) {
            trasactionState = false;
            return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);
        }else if(null != transaction){
            return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);
        }
        return new ResponseEntity<String>("Transaction Unavailable For Reference:" + transactionReference, HttpStatus.OK);
    }

    private ResponseEntity<?> checkStatusWithoutSms(String transactionReference) {
        log.info("updating without sms" + transactionReference);
        Transaction transaction = iTransactionService.getTransaction(transactionReference);
        if (null != transaction) {
            transaction.setStatus(TransactionStatus.Completed);
            transaction = iTransactionService.update(transaction);
            trasactionState = false;
            return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);
        }
        return new ResponseEntity<String>("Transaction Unavailable For Reference:" + transactionReference, HttpStatus.OK);
    }

    @PutMapping("/update")
    private ResponseEntity<Transaction> updateTransaction(@RequestBody TransactionDTO transactionDTO) {
        Transaction transaction = iTransactionService.updateTransaction(transactionDTO);
        return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);
    }
}
