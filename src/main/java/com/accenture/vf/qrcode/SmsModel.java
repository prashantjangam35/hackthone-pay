package com.accenture.vf.qrcode;


import lombok.Data;

@Data
public class SmsModel {
    private String subject;
    private String message;

}
