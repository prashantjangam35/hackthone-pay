package com.accenture.vf.qrcode.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public enum TransactionStatus {
    Initiated, Pending, Completed;

    private static final Set<String> transactionstatusset = new LinkedHashSet<String>(TransactionStatus.values().length);

    static {
        for (TransactionStatus statusEnum : TransactionStatus.values())
            transactionstatusset.add(statusEnum.name());
    }

    public static boolean contains(String value) {
        System.out.println(transactionstatusset);
        return transactionstatusset.contains(value);
    }
}
