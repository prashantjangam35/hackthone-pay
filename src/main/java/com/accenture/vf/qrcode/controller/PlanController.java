package com.accenture.vf.qrcode.controller;

import com.accenture.vf.qrcode.entity.Plan;
import com.accenture.vf.qrcode.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/plan")
@RestController
public class PlanController {

    @Autowired
    private PlanService planService;

    @PostMapping(value = "/create")
    private ResponseEntity<Plan> createPlan(@RequestBody Plan plan) {
        Plan savedplan = planService.createPlan(plan);
        return new ResponseEntity<Plan>(savedplan, HttpStatus.CREATED);
    }

    @GetMapping(value = "/findAll")
    public ResponseEntity<?> findAll() {
        List<Plan> plans = planService.getallplans();
        return new ResponseEntity<List<Plan>>(plans, HttpStatus.CREATED);
    }
}
