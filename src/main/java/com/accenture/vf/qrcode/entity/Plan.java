package com.accenture.vf.qrcode.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "plan")
@Entity
public class Plan {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String addOn;

    private String planHeading;

    private String price;

    private String planInfo;

    private String planValidity;

    private String helpText;
}
