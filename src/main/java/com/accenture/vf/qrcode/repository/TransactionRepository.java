package com.accenture.vf.qrcode.repository;

import com.accenture.vf.qrcode.entity.Transaction;
import com.accenture.vf.qrcode.entity.TransactionStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    public Transaction findByReference(String transactionreference);

    public List<Transaction> findByAmountAndStatus(Double amount, TransactionStatus status);

}
