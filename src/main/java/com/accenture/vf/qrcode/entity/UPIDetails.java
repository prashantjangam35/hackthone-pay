package com.accenture.vf.qrcode.entity;

import javax.validation.constraints.NotBlank;

public class UPIDetails {

    @NotBlank(message = "Enter Upi of the merchant.")
    private String vpa;
    @NotBlank(message = "Enter name of the merchant")
    private String name;

    private String txnReference;

    @NotBlank(message = "Enter the amount")
    private String amount;

    private String description;

    public String getVpa() {
        return vpa;
    }

    public void setVpa(String vpa) {
        this.vpa = vpa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTxnReference() {
        return txnReference;
    }

    public void setTxnReference(String txnReference) {
        this.txnReference = txnReference;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
