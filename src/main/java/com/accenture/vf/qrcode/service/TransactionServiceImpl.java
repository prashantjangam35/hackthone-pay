package com.accenture.vf.qrcode.service;

import com.accenture.vf.qrcode.entity.Transaction;
import com.accenture.vf.qrcode.entity.TransactionStatus;
import com.accenture.vf.qrcode.entity.TransactionType;
import com.accenture.vf.qrcode.repository.TransactionRepository;
import com.accenture.vf.qrcode.request.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class TransactionServiceImpl implements ITransactionService {
    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public Transaction createTransaction(TransactionDTO transactionDTO) {
        Transaction transaction = new Transaction();
        transaction.setReference(UUID.randomUUID().toString().substring(0, 30));
        transaction.setStatus(TransactionStatus.Initiated);
        transaction.setAmount(transactionDTO.getAmount());
        transaction.setDatetime(transactionDTO.getDatetime());
        transaction.setType(TransactionType.valueOf(transactionDTO.getType()));
        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction updateTransaction(TransactionDTO transactionDTO) {
        Transaction transaction = new Transaction();
        transaction.setId(transactionDTO.getId());
        transaction.setReference(transactionDTO.getReference());
        transaction.setStatus(TransactionStatus.Completed);
        transaction.setAmount(transactionDTO.getAmount());
        transaction.setPayerId(transactionDTO.getPayerId());
        transaction.setType(TransactionType.valueOf(transactionDTO.getType()));
        transaction.setRemark(transactionDTO.getRemark());
        transaction.setDatetime(LocalDateTime.now());
        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction update(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction getTransaction(String transactionreference) {
        return transactionRepository.findByReference(transactionreference);
    }

    @Override
    public Transaction getTransactionByAmount(String amount) {
        List<Transaction> list = transactionRepository.findByAmountAndStatus(Double.valueOf(amount), TransactionStatus.Initiated);
        if(!CollectionUtils.isEmpty(list)){
            return list.get(0);
        }
        return null;
    }
}
