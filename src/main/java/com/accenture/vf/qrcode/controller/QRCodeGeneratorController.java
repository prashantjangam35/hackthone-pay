package com.accenture.vf.qrcode.controller;

import com.accenture.vf.qrcode.entity.Plan;
import com.accenture.vf.qrcode.response.PaymentDetails;
import com.accenture.vf.qrcode.service.IQRCodeGeneratorService;
import com.google.zxing.WriterException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("/pay")
public class QRCodeGeneratorController {

    @Autowired
    private IQRCodeGeneratorService iQRCodeGeneratorService;

    @PostMapping(value = "/getqr", produces = {"application/json"})
    public PaymentDetails generateQRCode(@Valid @RequestBody Plan plan) throws IOException, WriterException {
        PaymentDetails paymentDetails = iQRCodeGeneratorService.generateQRCode(plan);
        return paymentDetails;
    }
}
