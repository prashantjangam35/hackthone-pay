package com.accenture.vf.qrcode.response;

import com.accenture.vf.qrcode.entity.Transaction;

import java.util.Arrays;

public class PaymentDetails {

    private byte[] imagedata;
    private Transaction transaction;

    public byte[] getImagedata() {
        return imagedata;
    }

    public void setImagedata(byte[] imagedata) {
        this.imagedata = imagedata;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return "PaymentDetails{" +
                "imagedata=" + Arrays.toString(imagedata) +
                ", transaction=" + transaction +
                '}';
    }
}
