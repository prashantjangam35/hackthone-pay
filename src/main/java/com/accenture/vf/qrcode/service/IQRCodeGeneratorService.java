package com.accenture.vf.qrcode.service;

import com.accenture.vf.qrcode.entity.Plan;
import com.accenture.vf.qrcode.response.PaymentDetails;
import com.google.zxing.WriterException;

import java.io.IOException;

public interface IQRCodeGeneratorService {

    public PaymentDetails generateQRCode(Plan plan) throws WriterException, IOException;
}
