package com.accenture.vf.qrcode.request;

import lombok.Data;

import java.time.LocalDateTime;


@Data
public class TransactionDTO {

    private Long id;

    private String reference;

    private Double amount;

    private String type;

    private String payerId;

    private String destination;

    private String status;

    private LocalDateTime datetime;

    private String remark;


}
