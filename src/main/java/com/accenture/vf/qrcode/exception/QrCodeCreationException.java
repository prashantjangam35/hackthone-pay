package com.accenture.vf.qrcode.exception;


public class QrCodeCreationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public QrCodeCreationException(String message) {
        super(message);
    }
}