package com.accenture.vf.qrcode.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Table(name = "transaction")
@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String reference;

    private Double amount;

    private TransactionType type;

    private String payerId;

    private String actualTransactionReference;

    private TransactionStatus status;

    private LocalDateTime datetime;

    private String remark;
}
