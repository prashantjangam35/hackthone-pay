package com.accenture.vf.qrcode.service;

import com.accenture.vf.qrcode.entity.Plan;
import com.accenture.vf.qrcode.entity.Transaction;
import com.accenture.vf.qrcode.entity.TransactionStatus;
import com.accenture.vf.qrcode.entity.TransactionType;
import com.accenture.vf.qrcode.request.TransactionDTO;
import com.accenture.vf.qrcode.response.PaymentDetails;
import com.accenture.vf.qrcode.utility.Constant;
import com.google.zxing.*;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class QRCodeGeneratorServiceImpl implements IQRCodeGeneratorService {
    private static final Map hintMap = new HashMap<>();
    private static final String charset = StandardCharsets.UTF_8.name(); // or "ISO-8859-1"
    private static final int size = 350;
    private static final Logger logger = LoggerFactory.getLogger(QRCodeGeneratorServiceImpl.class);
    @Autowired
    private ITransactionService iTransactionService;


    public QRCodeGeneratorServiceImpl() {
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        hintMap.put(EncodeHintType.CHARACTER_SET, StandardCharsets.UTF_8);
        hintMap.put(EncodeHintType.MARGIN, 1);

        hintMap.put(DecodeHintType.CHARACTER_SET, StandardCharsets.UTF_8);
        hintMap.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
        hintMap.put(DecodeHintType.POSSIBLE_FORMATS, EnumSet.allOf(BarcodeFormat.class));
    }

    public PaymentDetails generateQRCode(Plan plan) throws WriterException, IOException {


        if (null != plan) {
            TransactionDTO transactionDTO = new TransactionDTO();
            log.info(plan.getPrice());
            transactionDTO.setAmount(Double.parseDouble(plan.getPrice()));
            transactionDTO.setPayerId(Constant.SOURCE);
            transactionDTO.setDatetime(LocalDateTime.now());
            transactionDTO.setType(String.valueOf(TransactionType.UPI));
            transactionDTO.setStatus(String.valueOf(TransactionStatus.Initiated));
            Transaction transaction = iTransactionService.createTransaction(transactionDTO);

            final String qrCode = UriComponentsBuilder.fromUriString("upi://pay")
                    .queryParam("pa", Constant.MERCHENT_UPI_ID)
                    .queryParam("pn", Constant.MERCHENT_NAME)
                    .queryParam("tr", transaction.getReference())
                    .queryParam("am", transaction.getAmount())
                    .queryParam("cu", Constant.CURRENCY)
                    .queryParam("description", Constant.MERCHENT_DESCRIPTION)
                    .build().encode().toUriString();
            logger.info("UPI QR Code = " + qrCode);


            BitMatrix matrix = new MultiFormatWriter().encode(qrCode, BarcodeFormat.QR_CODE, size, size, hintMap);
            BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(matrix);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", byteArrayOutputStream);
            byte[] bytes = byteArrayOutputStream.toByteArray();

            PaymentDetails paymentDetails = new PaymentDetails();
            paymentDetails.setTransaction(transaction);
            paymentDetails.setImagedata(bytes);

            return paymentDetails;
        }
        return null;


    }


}
