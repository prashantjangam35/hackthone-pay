package com.accenture.vf.qrcode.service;


import com.accenture.vf.qrcode.entity.Transaction;
import com.accenture.vf.qrcode.request.TransactionDTO;

public interface ITransactionService {
    public Transaction createTransaction(TransactionDTO TransactionDTO);

    public Transaction updateTransaction(TransactionDTO TransactionDTO);

    public Transaction update(Transaction transaction);

    public Transaction getTransactionByAmount(String amount);

    public Transaction getTransaction(String transactionrefernce);
}
